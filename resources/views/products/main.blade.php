@extends('products.layout')

@section('content')
    <div class="card">
        <div class="card-header">
            <h2>Laravel 7 - Products CRUD by Livewire</h2>
        </div>
        <div class="card-body">
            @if (session()->has('message'))
                <div class="alert alert-success">
                    {{ session('message') }}
                </div>
            @endif
            @livewire('products')
        </div>
    </div>
@endsection
