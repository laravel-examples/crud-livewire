<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Product;

class Products extends Component
{
    public $products, $product_id, $name, $detail;
    public $updateMode = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public function render()
    {
        $this->products = Product::all();
        return view('products.products');
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    private function resetInputFields(){
        $this->name = '';
        $this->detail = '';
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public function store()
    {
        $validatedDate = $this->validate([
            'name' => 'required',
            'detail' => 'required',
        ]);
        Product::create($validatedDate);
        session()->flash('message', 'Product created successfully.');
        $this->resetInputFields();
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public function edit(Product $product)
    {
        $this->product_id = $product->id;
        $this->name = $product->name;
        $this->detail = $product->detail;
        $this->updateMode = true;
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public function cancel()
    {
        $this->updateMode = false;
        $this->resetInputFields();
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public function update(Product $product)
    {
        $validatedDate = $this->validate([
            'name' => 'required',
            'detail' => 'required',
        ]);
        $product->update($validatedDate);
        $this->updateMode = false;
        session()->flash('message', 'Product updated successfully.');
        $this->resetInputFields();
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public function delete(Product $product)
    {
        $product->delete();
        $this->updateMode = false;
        session()->flash('message', 'Product deleted successfully.');
        $this->resetInputFields();
    }
}
