<div>

    @if (session()->has('message'))
        <div class="alert alert-success">
            {{ session('message') }}
        </div>
    @endif

    @if($updateMode)
        @include('products.update')
    @else
        @include('products.create')
    @endif

    <hr>

    <table class="table table-bordered">
        <thead>
            <tr>
                <th>N°</th>
                <th>Name</th>
                <th>Detail</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach($products as $product)
            <tr>
                <td>{{ $product->id }}</td>
                <td>{{ $product->name }}</td>
                <td>{{ $product->detail }}</td>
                <td>
                    <button class="btn btn-primary btn-sm" wire:click.prevent="edit({{ $product->id }})">Edit</button>
                    <button class="btn btn-danger btn-sm" wire:click.prevent="delete({{ $product->id }})">Delete</button>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>

