<div class="row">
    <div class="col-lg-12">
        <div class="float-left">
            <h4>Create Product</h4>
        </div>
    </div>
</div>

<form>
    <div class="form-group">
        <label for="name"><strong>Name</strong></label>
        <input class="form-control" type="text" name="name" placeholder="Enter product name" wire:model="name">
        @error('name') <span class="text-danger">{{ $message }}</span> @enderror
    </div>
    <div class="form-group">
        <label for="detail"><strong>Details</strong></label>
        <textarea class="form-control" name="detail" placeholder="Enter product details" wire:model="detail"></textarea>
        @error('detail') <span class="text-danger">{{ $message }}</span> @enderror
    </div>
    <button class="btn btn-success" wire:click.prevent="store()">Save</button>
</form>
